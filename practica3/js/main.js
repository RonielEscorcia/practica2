$(document).ready(function(){

	$('p.show-stories').click(function(){
		$(this).siblings('div.stories').show();		
	});

	$('span.icon-menu').click(function(){

		$('div#menu').addClass('show-menu');
	});

	$('.icon-close').click(function(){
		$('div#menu').removeClass('show-menu');
	});

	$("a.accordion-title").click(function(e){
		
	   var contenido=$(this).next('div.accordion-content');		
	   if(contenido.css("display")=="none"){
	   	  $('div.accordion-content').slideUp(250);	
	      contenido.slideDown(250);			
	      $(this).addClass("open");
	   }
	   else{ 	
	      contenido.slideUp(250);
	      $(this).removeClass("open");	
	  }
	
		e.preventDefault();				
	});


	var id;
	$(window).resize(function() {
    	clearTimeout(id);
    	id = setTimeout(doneResizing, 500);

	});

	function doneResizing(){
  		var mq = window.matchMedia( "(min-width: 768px)" );

  		var boxOrange = $('section a.box-orange');
  		var destino;
  		if (mq.matches) {
  			destino = $('.box-blue').parent();
  			boxOrange.appendTo(destino);
		} else {
		  destino = $('section a.topic-seven').parent();
		  boxOrange.appendTo(destino);
		}
	}

	doneResizing();
});