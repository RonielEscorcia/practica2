$(document).ready(function(){

var f12h; // posicion vertical frame12
var f22h;
var f23h;
var id;


	$(window).resize(function() { // vigila el cambio de tamano del navegador
    	clearTimeout(id);
    	id = setTimeout(widthChange, 500);
	});


	function widthChange(){ // asigna separacion entre oraciones de animacion si cambia el tamaño del navegador
		if(matchMedia("(max-width: 700px)").matches){
			f12h = '160px';
			f22h = '110px';
			f23h = '160px';
		}else if(matchMedia("(max-width: 800px)").matches){
			f12h = '170px';
			f22h = '120px';
			f23h = '175px';
		}else if(matchMedia("(max-width: 900px)").matches){
			f12h = '180px';
			f22h = '130px';
			f23h = '190px';
		}else {
			f12h = '185px'; 
			f22h = '134px';
			f23h = '207px';
		}
	}


	widthChange();



		function animacionFrame1(){

			$('#f11').animate({ // Se empiezan a mostrar los primeros frames
				top: '100px',
				opacity: '1',
			}, 500, function(){
				$('#f12').animate({
					top: f12h,
					opacity: '1',
				}, 500, function(){ 
					setTimeout(function(){ // esperamos antes de ocultar del frame para que se pueda leer
						$('#f11').animate({
							top: '-10px',
							opacity: 0,
						}, 500, function(){
							$('#f12').animate({
								top: '-10px',
								opacity: 0,
							},500, animacionFrame2()); // al terminar ejecutamos el animacion de los frame2
						});
					},3000);					
				});
			});
		}

		function animacionFrame2() {
			$('#f21').animate({ // aparece los frames
				top: '67px',
				opacity: '1',
			}, 500, function(){
				$('#f22').animate({
					top: f22h,
					opacity: '1',
				}, 500, function(){
					$('#f23').animate({
						top: f23h,
						opacity: '1',
					}, 500, function(){
						setTimeout(function(){ // esperamos y ocultamos los frames para que se puedan leer
							$('#f21').animate({
								top: '-10px',
								opacity: 0,
							}, 500, function(){
								$('#f22').animate({
									top: '-10px',
									opacity: 0,
								},500, function(){
									$('#f23').animate({
										top: '-10px',
										opacity: 0,
									}, 500, animacionFrame3());
								});
							});
						},3000);
					});										
				});
			});
		}

		function animacionFrame3(){
			var ancho = 0;
			$('#f31').animate({
				top: '76px',
				opacity: '1',
			}, 500, function(){
				var ancho = 138 / 1000 * 100;
				$('.linea').animate({
					width: ancho + '%',
				}, 500, function(){
					$('.animado').css('display','block');
					$('#erat').animate({
						opacity: '1',
					},250, function(){
						ancho = 257 / 1000 * 100;
						$('.linea').animate({
							width: ancho + '%',
						}, 250, function(){
							$('#dui').animate({
								opacity: '1',
							}, 250, function(){
								ancho = 349 / 1000 * 100;
								$('.linea').animate({
									width: ancho + '%',
								}, 250, function(){
									$('#sed').animate({
										opacity: '1',
									}, 250, function(){
										ancho = 627 / 1000 * 100;
										$('.linea').animate({
											width: ancho + '%',
										}, 500, function(){
											setTimeout(function(){
												$('.frame1 img').css({
													top: '335px',
													opacity: '0',
												});
												$('.frame2 img').css({
													top: '335px',
													opacity: '0',
												});

												$('#f31').css({
													top: '355px',
													opacity: '0',
												});

												$('.animado').css('display', 'none');
												$('div.animado img').css('opacity', '0');
												$('.linea').css('width', '0px');

												animacionFrame1();
											}, 3000);
										});
									});
								});
							});
						});
					});
				});
				
			});
		}

		animacionFrame1();
});
